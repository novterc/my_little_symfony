#Test work "My little symfony"
For start the project, follow the next steps:

```
git clone https://novterc@bitbucket.org/novterc/my_little_symfony.git
```
```
cd my_little_symfony
```
```
Change settings for connecting to the database in config.php 
```
```
composer install
```
```
php vendor/bin/doctrine orm:schema-tool:update --force
```
Good luck!