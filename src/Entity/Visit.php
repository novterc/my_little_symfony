<?php

namespace src\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Visit
 *
 * @ORM\Table(name="visit")
 * @ORM\Entity
 */
class Visit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="ip_address", type="string", length=50, nullable=true)
     */
    private $ipAddress;

    /**
     * @ORM\Column(name="user_agent", type="string", length=255, nullable=true)
     */
    private $userAgent;

    /**
     * @ORM\Column(name="view_date", type="datetime", nullable=true)
     */
    private $viewData;

    /**
     * @ORM\Column(name="page_url", type="string", length=255, nullable=true)
     */
    private $pageUrl;

    /**
     * @ORM\Column(name="views_count", type="integer", length=11, nullable=true)
     */
    private $viewsCount;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set IP address
     * @param string $ipAddress
     * @return Visit
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get IP address
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set user agent
     * @param string $userAgent
     * @return Visit
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get user agent
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set view data
     * @param \DateTime $viewData
     * @return Visit
     */
    public function setViewData($viewData)
    {
        $this->viewData = $viewData;

        return $this;
    }

    /**
     * Get view data
     * @return string
     */
    public function getViewData()
    {
        return $this->viewData;
    }

    /**
     * Set page url
     * @param string $pageUrl
     * @return Visit
     */
    public function setPageUrl($pageUrl)
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * Get page url
     * @return string
     */
    public function getPageUrl()
    {
        return $this->pageUrl;
    }

    /**
     * Set views count
     * @param string $viewsCount
     * @return Visit
     */
    public function setViewsCount($viewsCount)
    {
        $this->viewsCount = $viewsCount;

        return $this;
    }

    /**
     * Get views count
     * @return string
     */
    public function getViewsCount()
    {
        return $this->viewsCount;
    }

}
