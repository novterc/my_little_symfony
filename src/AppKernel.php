<?php

namespace src;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use src\Controller\BannerController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppKernel
{
    protected $config;
    protected $isDevMode;
    protected $entityManager;

    /**
     * AppKernel constructor.
     * @param array
     * @param boolean $devMode
     */
    public function __construct(array $config, bool $isDevMode = false)
    {
        $this->config = $config;
        $this->isDevMode = $isDevMode;
    }

    /**
     * Get config data by path
     * @param string $path
     * @return array|mixed
     * @throws \Exception
     */
    public function getConfig($path = '')
    {
        $keyArr = explode('.', $path);
        $config = $this->config;
        if (!empty($keyArr)) {
            foreach ($keyArr as $key) {
                if (!isset($key)) {
                    throw new \Exception('not found config path:' . $path);
                }
                $config = $config[$key];
            }
        }

        return $config;
    }

    /**
     * Get mode status
     * @return bool
     */
    public function isDevMode()
    {
        return $this->isDevMode;
    }

    /**
     * Get entity manager instance
     * @return EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $config = Setup::createAnnotationMetadataConfiguration($this->getConfig('paths.entity'), $this->isDevMode(), null, null, false);
            $entityManager = EntityManager::create($this->getConfig('dbParams'), $config);
            $this->entityManager = $entityManager;
        }

        return $this->entityManager;
    }

    /**
     * Handle http request instance
     * @param Request $request
     * @return Response
     */
    public function handlerHtppRequest(Request $request)
    {
        try {
            $bannderController = new BannerController($this->getEntityManager());
            $response = $bannderController->indexAction($request);
        } catch (\Exception $exception) {
            $response = new Response();
            $response->setStatusCode(500);
            if ($this->isDevMode()) {
                $response->setContent($exception->getMessage() . '-' . $exception->getFile() . ':' . $exception->getLine());
            }
        }

        return $response;
    }

}
