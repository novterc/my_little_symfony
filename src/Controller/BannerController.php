<?php

namespace src\Controller;

use Doctrine\ORM\EntityManager;
use src\Entity\Visit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;

class BannerController
{
    protected $em;

    /**
     * BannerController constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Index action
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        if (!$this->validateRequest($request)) {
            $response = new Response();
            $response->setStatusCode(400);
            return $response;
        }

        $this->handleRequestByVisit($request);

        $filePath = __DIR__ . '/../../resources/image.png';

        return $this->getResponseForFileUpload($filePath);
    }

    /**
     * Get Response instance for upload file by path
     * @param string $filePath
     * @return Response
     */
    protected function getResponseForFileUpload(string $filePath)
    {
        $response = new Response();
        $response->headers->set('Content-type', mime_content_type($filePath));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filePath) . '";');
        $response->headers->set('Content-length', filesize($filePath));
        $response->headers->set('Content-Transfer-Encoding', "binary");

        return $response->setContent(file_get_contents($filePath));
    }

    /**
     * Create or inc visit by request
     * @param Request $request
     */
    protected function handleRequestByVisit(Request $request)
    {
        $visitRepository = $this->em->getRepository(Visit::class);
        $visits = $visitRepository->findBy([
            'ipAddress' => $request->getClientIp(),
            'userAgent' => $request->headers->get('User-Agent'),
            'pageUrl' => $request->headers->get('referer'),
        ]);
        if (!empty($visits)) {
            foreach ($visits as $visit) {
                $count = (int)$visit->getViewsCount();
                $count++;
                $visit->setViewsCount($count);
                $this->em->persist($visit);
            }
        } else {
            $visit = new Visit();
            $visit->setViewData(new \DateTime());
            $visit->setIpAddress($request->getClientIp());
            $visit->setUserAgent($request->headers->get('User-Agent'));
            $visit->setPageUrl($request->headers->get('referer'));
            $visit->setViewsCount(1);
            $this->em->persist($visit);
        }

        $this->em->flush();
    }

    /**
     * Validate request for visit
     * @param Request $request
     * @return bool
     */
    protected function validateRequest(Request $request)
    {
        $validator = Validation::createValidator();

        $violations = $validator->validate($request->getClientIp(), [
            new Length(array('max' => 50)),
        ]);
        if (count($violations) !== 0) {
            return false;
        }

        $violations = $validator->validate($request->headers->get('User-Agent'), [
            new Length(array('max' => 255)),
        ]);
        if (count($violations) !== 0) {
            return false;
        }

        $violations = $validator->validate($request->headers->get('referer'), [
            new Length(array('max' => 255)),
        ]);
        if (count($violations) !== 0) {
            return false;
        }

        return true;
    }
}
