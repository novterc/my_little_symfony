<?php

use src\AppKernel;

require_once "bootstrap.php";

$config = include __DIR__ . '/config.php';
$kernel = new AppKernel($config, true);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($kernel->getEntityManager());